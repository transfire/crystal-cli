
macro cli(topts)
  cli(ARGV, topts)
end

macro cli(targs, topts)
  begin
    # Convert single dash flags into multiple flags, e.g. "-abc" to "-a", "-b", "-c"
    args = [] of String
    {{targs}}.each do |v|
      if v[0,1] == "-" && v[1,1] != "-"
        args.concat(v[1..-1].chars.map{|c| "-#{c}"})
      else
        args << v
      end
    end

    while args.any?
      item = args.shift
      {% for kst, v in topts %}
        # split topts, e.g. "-f --file" to "-f", "--file"
        {% ks = kst.split(/\s+/) %}
        {% for k in ks %}
          if item == {{ k }}
            mproc = ->{{ v }}
            arity = [mproc.arity, 0].max
            {% if v.id.ends_with?(")") %}
              t = Tuple.new
              {% sigs = v.id.gsub(/^.*\(/, "")[0..-2].split(",") %}
              {% for sig in sigs %}
                #p {{ sig }}
                {% if sig == "String" %}
                  t = t + Tuple(String).new(args.shift)
                {% elsif sig == "Int8" %}
                  t = t + Tuple(Int8).new(args.shift.to_i8)
                {% elsif sig == "Int16" %}
                  t = t + Tuple(Int16).new(args.shift.to_i16)
                {% elsif sig == "Int32" %}
                  t = t + Tuple(Int32).new(args.shift.to_i32)
                {% elsif sig == "Int64" %}
                  t = t + Tuple(Int64).new(args.shift.to_i64)
                {% elsif sig == "UInt8" %}
                  t = t + Tuple(UInt8).new(args.shift.to_u8)
                {% elsif sig == "UInt16" %}
                  t = t + Tuple(UInt16).new(args.shift.to_u16)
                {% elsif sig == "UInt32" %}
                  t = t + Tuple(UInt32).new(args.shift.to_u32)
                {% elsif sig == "UInt64" %}
                  t = t + Tuple(UInt64).new(args.shift.to_u64)
                {% elsif sig == "Float32" %}
                  t = t + Tuple(Float32).new(args.shift.to_f32)
                {% elsif sig == "Float64" %}
                  t = t + Tuple(Float64).new(args.shift.to_f64)
                {% end %}
              {% end %}
              mproc.call(*t)
            {% else %}
              mproc.call()
            {% end %}
          end
        {% end %}
      {% end %}
    end
  end
end

