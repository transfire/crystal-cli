
  # From original Ruby Version

  def cli(**opts)
    cli(ARGV, **opts)
  end

  def cli(targv, **topts)
    args = [] of String

    # Split option aliases
    opts = {} of String => Proc(Array(String), String)
    topts.each do |k,v|
      k.to_s.split(/\s+/).each do |o|
        opts[o] = v
      end
    end

    # Convert single dash flags into multiple flags.
    argv = [] of String
    targv.each do |v|
      if v[0,1] == '-' && v[1,1] != '-'
        argv.concat(v[1..-1].chars.map{|c| "-#{c}"})
      else
        argv << v
      end
    end

    while argv.any?
      item = argv.shift
      flag = opts[item]

      if flag
        # Work around lambda semantics in 1.8.7.
        arity = [flag.arity, 0].max

        # Raise if there are not enough parameters
        # available for the flag.
        if argv.size < arity
          raise(ArgumentError.new("Not enough parameters for #{item}."))
        end

        # Call the lambda with N items from argv,
        # where N is the lambda's arity.
        flag.call(argv.shift(arity))
      else

        # Collect the items that don't correspond to
        # flags.
        args << item
      end
    end

    # TODO: should we replace the argv given with args?

    args
  end

