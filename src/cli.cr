require "./cli/*"

# To use the `cli` macro, include the CLI module into you code.
#
#     include CLI
#
# Then call the macro mapping flags and subcommands to methods. Be sure to
# pass the macro ARGV, for production use.
#
#     def run_cli(argv=ARGV)
#       cli argv, {
#         "-o --output": set_output(String),
#         "-h --help": show_help;
#       }
#     end
#
# To use the `#ask` method be sure to include it directly.
#
#     require "cli/ask"
#
#     ans = "Do you wanna? [Y/n]", "Y"
#
# NOTE: This is an early version, so please report any edge cases that
# need to be shored up, and offer suggestions for improvement. Thanks.
#
module CLI

  # The cli macro in which the arguments array to parse must be passed.
  #
  macro cli(targs, topts)
    begin
      argr = [] of String

      # Convert single dash flags into multiple flags, e.g. "-abc" to "-a", "-b", "-c"
      args = [] of String
      {{targs}}.each do |v|
        if v[0,1] == "-" && v[1,1] != "-"
          args.concat(v[1..-1].chars.map{|c| "-#{c}"})
        else
          args << v
        end
      end

      while args.any?
        item = args.shift
        item_matched = false
        {% for kst, m in topts %}
          # split topts, e.g. "-f --file" to "-f", "--file"
          {% ks = kst.strip.split(" ") %}
          {% for k in ks %}  
            {% k = k.strip %}  
            if item == {{ k }}
              item_matched = true
              mproc = ->{{ m }}
              arity = [mproc.arity, 0].max
              {% if !k.starts_with?("-") %}
                # subcommand
                mproc.call(args.dup)
              {% else %}
                {% if m.id.ends_with?(")") %}
                  t = Tuple.new
                  {% sigs = m.id.gsub(/^.*\(/, "")[0..-2].split(",") %}
                  {% for sig in sigs %}
                    {% if sig == "String" %}
                      t = t + Tuple(String).new(args.shift)
                    {% elsif sig == "Int8" %}
                      t = t + Tuple(Int8).new(args.shift.to_i8)
                    {% elsif sig == "Int16" %}
                      t = t + Tuple(Int16).new(args.shift.to_i16)
                    {% elsif sig == "Int32" %}
                      t = t + Tuple(Int32).new(args.shift.to_i32)
                    {% elsif sig == "Int64" %}
                      t = t + Tuple(Int64).new(args.shift.to_i64)
                    {% elsif sig == "UInt8" %}
                      t = t + Tuple(UInt8).new(args.shift.to_u8)
                    {% elsif sig == "UInt16" %}
                      t = t + Tuple(UInt16).new(args.shift.to_u16)
                    {% elsif sig == "UInt32" %}
                      t = t + Tuple(UInt32).new(args.shift.to_u32)
                    {% elsif sig == "UInt64" %}
                      t = t + Tuple(UInt64).new(args.shift.to_u64)
                    {% elsif sig == "Float32" %}
                      t = t + Tuple(Float32).new(args.shift.to_f32)
                    {% elsif sig == "Float64" %}
                      t = t + Tuple(Float64).new(args.shift.to_f64)
                    {% end %}
                  {% end %}
                  mproc.call(*t)
                {% else %}
                  mproc.call()
                {% end %}
              {% end %}
            end
          {% end %}
        {% end %}

        if !item_matched
          if item.starts_with?("-")
            raise ArgumentError.new("Not an option: " + item)
          else
            argr << item
          end
        end
      end
    end

    argr
  end

end
