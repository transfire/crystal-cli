require "./spec_helper"

class CLITest
  include CLI

  def initialize
    @a = ""
    @b = ""
    @c = 0
    @d = ""
    @e = 0.0
    @s = [] of String
  end

  def a; @a; end
  def b; @b; end
  def c; @c; end
  def d; @d; end
  def s; @s; end

  def a(v : String)
    @a = v
  end

  def b!
    @b = "B"
  end

  def c(i : Int32)
    @c = i
  end

  def dx
    @d = "dx"
  end

  def s(args : Array(String))
    @s = args
  end

  def run(argv : Array(String))
    cli argv, {
          "s": s(Array(String)),
         "-b": b!,
         "-a": a(String),
        "--a": a(String),
        "--c": c(Int32),
        "-d --dx": dx
    }
  end
end

describe CLI do
  describe "#cli" do

    it "correctly handles single dash flag" do
      ct = CLITest.new
      ct.run ["-b"]
      ct.b.should eq "B"
    end

    it "correctly handles single dash flag with argument" do
      ct = CLITest.new
      ct.run ["-a", "A"]
      ct.a.should eq "A"
    end

    it "correctly handles double dash flag with argument" do
      ct = CLITest.new
      ct.run ["--a", "AAA"]
      ct.a.should eq "AAA"
    end

    it "correctly handles multi single dash flags" do
      ct = CLITest.new
      ct.run ["-ba", "A"]
      ct.a.should eq "A"
      ct.b.should eq "B"
    end

    it "correctly handle integer converstion" do
      ct = CLITest.new
      ct.run ["--c", "13"]
      ct.c.should eq 13
    end

    it "correctly handle multiple flags 1" do
      ct = CLITest.new
      ct.run ["-d"]
      ct.d.should eq "dx"
    end

    it "correctly handle multiple flags 2" do
      ct = CLITest.new
      ct.run ["--dx"]
      ct.d.should eq "dx"
    end

    it "correctly handle subcommand" do
      ct = CLITest.new
      ct.run ["s", "extra"]
      ct.s.should eq ["extra"]
    end

  end
end
